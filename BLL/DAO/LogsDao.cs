﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using BLL.Entities;

namespace BLL.DAO
{
    public class LogsDao
    {
        public static string LastError = "";

        public static int Insert(Log log)
        {
            string query = @"INSERT INTO [dbo].[Logs] " +
                           "([SelfiesId],[Message],[Tweet],[TwitterId],[Type],[IP],[CreatedAt],[CreatedBy]) " +
                           "VALUES " +
                           "(@SelfiesId,@Message,@Tweet,@TwitterId,@Type,@IP,@CreatedAt,@CreatedBy) " +
                           "SELECT CAST(scope_identity() AS int)";
            try
            {
                var p1 = new SqlParameter("@IP", (string.IsNullOrEmpty(log.IP) ? "" : log.IP));
                var p2 = new SqlParameter("@SelfiesId", (log.SelfiesId == 0 ? 0 : log.SelfiesId));
                var p3 = new SqlParameter("@Tweet", (string.IsNullOrEmpty(log.Tweet) ? "" : log.Tweet));
                var p4 = new SqlParameter("@Message", (string.IsNullOrEmpty(log.Message) ? "" : log.Message));
                var p5 = new SqlParameter("@TwitterId", (string.IsNullOrEmpty(log.TwitterId) ? "" : log.TwitterId));
                var p6 = new SqlParameter("@Type", log.Type);
                var p7 = new SqlParameter("@CreatedAt", DateTime.Now);
                var p8 = new SqlParameter("@CreatedBy", log.CreatedBy);

                var list = new[] { p1, p2, p3, p4, p5, p6, p7, p8 };
                var cmd = new SqlCommand { CommandText = query, CommandType = CommandType.Text };
                cmd.Parameters.AddRange(list);
                Connection.OpenConnection();
                cmd.Connection = Connection.DbConnection;
                int result = (int)cmd.ExecuteScalar();
                Connection.CloseConnection();
                cmd.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                string msg = ("LogsDao Insert(Log log) " + ex.Message);
                return 0;
            }
            finally
            { Connection.CloseConnection(); }
        }

        public static Log Get(int id)
        {
            LastError = "";
            var log = new Log();
            try
            {
                const string sql = "Select * FROM Logs WHERE ID=@ID";
                var p1 = new SqlParameter("@ID", id);
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.Add(p1);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());

                if (dr.Count > 0) log = dr.FirstOrDefault();

                Connection.CloseConnection();
                return log;
            }
            catch (Exception ex)
            {
                string msg = string.Format("LogsDao Get(int id) id={0}<br />{1}", id, ex.Message);
                return log;
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static List<Log> GetList()
        {
            LastError = "";
            try
            {
                string sql = "Select * FROM Logs ORDER BY ID DESC";
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());
                Connection.CloseConnection();
                return dr;
            }
            catch (Exception ex)
            {
                string msg = string.Format("LogsDao GetList() <br />{1}", ex.Message);
                return new List<Log>();
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        private static List<Log> GetListFromDataReader(SqlDataReader dr)
        {
            LastError = "";
            List<Log> logs = new List<Log>();
            try
            {
                while (dr.Read())
                {
                    var log = new Log();
                    if (!dr.IsDBNull(dr.GetOrdinal("ID")))
                        log.ID = dr.GetInt32(dr.GetOrdinal("ID"));
                    if (!dr.IsDBNull(dr.GetOrdinal("Type")))
                        log.Type = dr.GetInt32(dr.GetOrdinal("Type"));

                    if (!dr.IsDBNull(dr.GetOrdinal("TwitterId")))
                        log.TwitterId = dr["TwitterId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("SelfiesId")))
                        log.SelfiesId = dr.GetInt32(dr.GetOrdinal("SelfiesId"));
                    if (!dr.IsDBNull(dr.GetOrdinal("IP")))
                        log.IP = dr["IP"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("CreatedBy")))
                        log.CreatedBy = dr["CreatedBy"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Tweet")))
                        log.Tweet = dr["Tweet"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("Message")))
                        log.Message = dr["Message"].ToString();

                    if (!dr.IsDBNull(dr.GetOrdinal("CreatedAt")))
                        log.CreatedAt = dr.GetDateTime(dr.GetOrdinal("CreatedAt"));

                    logs.Add(log);
                }
                dr.Close();
                dr.Dispose();
            }
            catch (Exception ex)
            {
                LastError = string.Format("LogsDao GetListFromDataReader(SqlDataReader dr)<br />" + ex.ToString());
            }
            return logs;
        }
    }
}
