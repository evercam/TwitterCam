﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using BLL.Entities;

namespace BLL.DAO
{
    public class TwitterDao
    {
        public static string LastError = "";

        public static Twitter Get(string twitterId)
        {
            LastError = "";
            var twitter = new Twitter();
            try
            {
                const string sql = "Select * FROM Twitters WHERE ID=@ID AND IsActive=1";
                var p1 = new SqlParameter("@ID", twitterId);
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Parameters.Add(p1);
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());

                if (dr.Count > 0) twitter = dr.FirstOrDefault();

                Connection.CloseConnection();
                return twitter;
            }
            catch (Exception ex)
            {
                LastError = string.Format("TwitterDao Get(string evercamId, string twitterId) twitterId={0}<br />{1}", twitterId, ex.Message);
                return twitter;
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        public static List<Twitter> GetList()
        {
            LastError = "";
            try
            {
                string sql = "Select * FROM Twitters";
                var cmd = new SqlCommand { CommandText = sql, CommandType = CommandType.Text };
                cmd.Connection = Connection.DbConnection;
                Connection.OpenConnection();
                var dr = GetListFromDataReader(cmd.ExecuteReader());
                Connection.CloseConnection();
                return dr;
            }
            catch (Exception ex)
            {
                LastError = string.Format("TwitterDao GetList() <br />{1}", ex.Message);
                return new List<Twitter>();
            }
            finally
            {
                Connection.CloseConnection();
            }
        }

        private static List<Twitter> GetListFromDataReader(SqlDataReader dr)
        {
            LastError = "";
            List<Twitter> twitters = new List<Twitter>();
            try
            {
                while (dr.Read())
                {
                    var twitter = new Twitter();

                    if (!dr.IsDBNull(dr.GetOrdinal("Id")))
                        twitter.ID = dr["Id"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("AccessToken")))
                        twitter.AccessToken = dr["AccessToken"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("TokenSecret")))
                        twitter.TokenSecret = dr["TokenSecret"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("EvercamId")))
                        twitter.EvercamID = dr["EvercamId"].ToString();
                    if (!dr.IsDBNull(dr.GetOrdinal("IsActive")))
                        twitter.IsActive = dr.GetBoolean(dr.GetOrdinal("IsActive"));

                    twitters.Add(twitter);
                }
                dr.Close();
                dr.Dispose();
            }
            catch (Exception ex)
            {
                LastError = string.Format("TwitterDao GetListFromDataReader(SqlDataReader dr)<br />" + ex.ToString());
            }
            return twitters;
        }
    }
}
