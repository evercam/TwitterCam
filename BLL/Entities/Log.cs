﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Entities
{
    public class Log
    {
        public int ID { get; set; }
        public string CreatedBy { get; set; }
        public string Message { get; set; }
        public string Tweet { get; set; }
        public int SelfiesId { get; set; }
        public string TwitterId { get; set; }
        public string IP { get; set; }
        public int Type { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
