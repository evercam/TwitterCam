﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Entities
{
    public class Twitter
    {
        public string ID { get; set; }
        public string AccessToken { get; set; }
        public string TokenSecret { get; set; }
        public string EvercamID { get; set; }
        public bool IsActive { get; set; }
    }
}
