﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Threading;
using Newtonsoft.Json;
using Tweetinvi;
using EvercamV2;
using BLL;
using BLL.DAO;
using BLL.Entities;
using AppModule.InterProcessComm;
using AppModule.NamedPipes;

namespace Listener
{
    class Program
    {
        static int RETRY_INTERVAL = int.Parse(ConfigurationSettings.AppSettings["RetryInterval"]);
        static int TRY_COUNT = int.Parse(ConfigurationSettings.AppSettings["TryCount"]);
        static Twitter twitter = null;
        static string twid = "";
        static IInterProcessConnection clientConnection = null;
        static Tweetinvi.Core.Interfaces.ILoggedUser user = null;
        static Evercam Evercam = new Evercam();
        public static Dictionary<string, BLL.Entities.Snapshot> _Snapshots = new Dictionary<string, BLL.Entities.Snapshot>();

        public delegate void ProcessTweetDelegate(Tweetinvi.Core.Events.EventArguments.TweetReceivedEventArgs args);

        static void Main(string[] param)
        {
            //// for testing purpose only
            //param = new string[] { "eapptester", "0" };

            Thread.Sleep(int.Parse(param[1]) * 1000);
            try
            {
                twid = param[0];
                Console.Title = "@" + twid;

                //// INITIALIZING CONNECTION WITH NAMED PIPE SERVER
                clientConnection = new ClientPipeConnection(Utils.ServerPipeName, ".");

                twitter = TwitterDao.Get(twid);
                if (!string.IsNullOrEmpty(SelfiesDao.LastError))
                    Utils.DBLog(LogTypes.Error, "TwitterDao.Get(" + twid + ") [Error] " + SelfiesDao.LastError, twid, "Listener");

                if (twitter == null || string.IsNullOrEmpty(twitter.ID))
                {
                    Utils.DBLog(LogTypes.Debug, "@" + twid + ": Twitter account details not found", twid, "Listener");
                    ServerPost("Error#1: Twitter account @" + twid + " details not found");
                    Environment.Exit(0);
                }

                if (!twitter.IsActive)
                {
                    Utils.DBLog(LogTypes.Debug, "@" + twid + ": Twitter account disabled", twid, "Listener");
                    ServerPost("Error#1: Twitter account @" + twid + " disabled.");
                    Environment.Exit(0);
                }

                if (string.IsNullOrEmpty(twitter.AccessToken) || string.IsNullOrEmpty(twitter.TokenSecret))
                {
                    Utils.DBLog(LogTypes.Debug, "@" + twid + ": Twitter account has invalid credentials", twid, "Listener");
                    ServerPost("Error#2: Twitter account @" + twid + " has invalid credentials.");
                    Environment.Exit(0);
                }

                // preparing credentials for given twitter user
                TwitterCredentials.ApplicationCredentials = TwitterCredentials.CreateCredentials(
                    twitter.AccessToken,        //  replace this with twitter token
                    twitter.TokenSecret,        //  replace this with twitter token secret
                    Utils.ConsumerKey,
                    Utils.ConsumerSecret);

                user = Tweetinvi.User.GetLoggedUser(TwitterCredentials.ApplicationCredentials);
                if (user == null)
                {
                    Utils.DBLog(LogTypes.Error, "Unable to connect to twitter [Error-3] " + ExceptionHandler.GetLastException().TwitterExceptionInfos.First().Message, twid, "Listener");
                    ServerPost("Error#3: Unable to connect to twitter.");
                    Environment.Exit(0);
                }

                Evercam.SANDBOX = bool.Parse(ConfigurationSettings.AppSettings["EvercamSandboxMode"]);
                // initially setting up evercam with app credentials
                Evercam = new Evercam(
                    ConfigurationSettings.AppSettings["EvercamClientID"],
                    ConfigurationSettings.AppSettings["EvercamClientSecret"],
                    ConfigurationSettings.AppSettings["EvercamClientUri"]);

                Utils.DBLog(LogTypes.Info, "Starting twitter stream listener for @" + user.ScreenName, twid, "Listener");

                //// handshaking with server
                Console.WriteLine("@" + twid + ": hi");

                // Enable Tweetinvi RateLimit Handler
                RateLimit.UseRateLimitAwaiter = true;

                // Get notified when your application is being stopped to wait for RateLimits to be available
                RateLimit.QueryAwaitingForRateLimit += (sender, args) =>
                {
                    Utils.DBLog(LogTypes.Debug, "@" + twid + " is awaiting " + args.ResetInMilliseconds + "ms for RateLimit to be available", twid, "Listener");
                };

                TwitterCredentials.ExecuteOperationWithCredentials(
                    TwitterCredentials.ApplicationCredentials, () =>
                    {
                        #region User Stream

                        var userStream = Tweetinvi.Stream.CreateUserStream();

                        userStream.TweetCreatedByAnyone += (sender, args) =>
                        {
                            ProcessTweetDelegate processTweet = new ProcessTweetDelegate(ProcessTweet);
                            processTweet(args);
                        };

                        userStream.StreamStopped += (sender, args) =>
                        {
                            string message = "@" + twid + " Stream stopped... [Error-4] ";
                            if (args.DisconnectMessage != null && args.DisconnectMessage.Code == 503)
                            {
                                message += args.DisconnectMessage.StreamName + " [" + args.DisconnectMessage.Code + "] " + args.DisconnectMessage.Reason;
                                ServerPost("Error#4: Stream stopped... " + args.DisconnectMessage.Reason);
                            }
                            else
                            {
                                Utils.DBLog(LogTypes.Info, message, twid, "Listener");
                                ServerPost("Error#4: Stream stopped... ");
                            }

                            Utils.SendMail("@" + twid + "Stream Stopped... [UTC] " + DateTime.UtcNow, message, Utils.DeveloperEmail);
                            //// just exit this process so that server may restart it instead of resuming
                            //userStream.ResumeStream();
                            try
                            {
                                // make sure
                                userStream.StopStream();
                            }
                            catch (Exception x)
                            {
                                Utils.DBLog(LogTypes.Error, "[Error-5] " + x.ToString(), twid, "Listener");
                            }
                            try
                            {
                                // try restart
                                userStream.ResumeStream();
                            }
                            catch (Exception x)
                            {
                                Utils.DBLog(LogTypes.Error, "[Error-6] " + x.ToString(), twid, "Listener");
                            }
                        };

                        //// STARTING STREAM
                        userStream.StartStream();

                        #endregion User Stream
                    });

                Utils.DBLog(LogTypes.Info, "Exiting listener...", twid, "Listener");
            }
            catch (Exception x)
            {
                Utils.DBLog(LogTypes.Error, "[Error-7] " + x.ToString(), twid, "Listener");
                ServerPost("Error#7: " + x.Message);
            }
            finally
            {
                ServerPost("Exiting...");
                clientConnection.Dispose();

                // cleanly exit the process and let the server restart it
                Environment.Exit(0);
            }
        }

        public static void ProcessTweet(Tweetinvi.Core.Events.EventArguments.TweetReceivedEventArgs args)
        {
            //// if user has mentioned no @username then quit
            if (args.Tweet.UserMentions.Count == 0)
                return;

            //// if this is a reply to an existing tweet then quit
            if (!string.IsNullOrEmpty(args.Tweet.InReplyToStatusIdStr))
                return;

            //// if this tweet is created by Evercam.Selfies app
            if (args.Tweet.Source.Contains(Utils.AppName))
                return;

            //// this may cause extra hits on db but required
            //twitter = TwitterDao.Get(param[0]);

            //// if user has not provided any response template then quit
            if (twitter == null || string.IsNullOrEmpty(twitter.ID))
            {
                Utils.DBLog(LogTypes.Debug, "@" + twid + ": Invalid Twitter account details", twid, "Listener");
                Environment.Exit(0);
            }

            Console.WriteLine("@" + args.Tweet.Creator.ScreenName + ": " + args.Tweet.Text);

            //// considering that ONLY first @mention in the tweet should reply to it
            if (args.Tweet.UserMentions[0].ScreenName.Equals(user.ScreenName))
            {
                var creator = Tweetinvi.User.GetUserFromId(args.Tweet.Creator.Id);

                TweetStatus s = new TweetStatus()
                {
                    Tid = args.Tweet.Id,
                    Message = args.Tweet.Text,
                    Tags = UniqueTags(args.Tweet.Hashtags),
                    Mentions = UniqueMentions(args.Tweet.UserMentions),
                    Creator = args.Tweet.Creator.ScreenName,
                    Listener = user.ScreenName
                };

                string post = JsonConvert.SerializeObject(s);
                string get = ServerPost("@" + user.ScreenName + ": " + post);

                //// normal reply from server
                if (get.StartsWith("@"))
                {
                    Console.WriteLine(Utils.ServerName + ": " + get);
                    return;
                }

                List<Selfies> selfies = new List<Selfies>();
                //// an array of selfies returned from server
                if (get.StartsWith("[{"))
                {
                    selfies = JsonConvert.DeserializeObject<List<Selfies>>(get);
                    string list = "";
                    foreach (Selfies self in selfies)
                    {
                        if (!string.IsNullOrEmpty(self.HashTag))
                            list += self.TwitterID + self.HashTag + ", ";
                        else
                            list += self.TwitterID + ", ";
                    }
                    Console.WriteLine(Utils.ServerName + ": " + list);
                }
                else
                {
                    Console.WriteLine(Utils.ServerName + ": No Response...");
                }

                if (selfies.Count == 0)
                {
                    if (Utils.TweetWithoutImage == "true")
                    {
                        TwitterCredentials.SetCredentials(twitter.AccessToken, twitter.TokenSecret, Utils.ConsumerKey, Utils.ConsumerSecret);
                        Tweet.PublishTweet("Hey @" + args.Tweet.Creator.ScreenName + ", Please provide #hashtag for any camera as well");
                    }
                    Console.WriteLine("No default selfies found for @" + twitter.ID + Environment.NewLine);
                }
                else
                {
                    string allmentions = "";
                    string[] mentions = UniqueMentions(args.Tweet.UserMentions);

                    //// replaces first @mention (our replier) with the tweet @creator
                    mentions[0] = creator.ScreenName;

                    //// recreate allmentions with all mentions in a tweet, including @creator
                    foreach (var m in mentions)
                    {
                        if (!allmentions.Contains("@" + m + " "))
                            allmentions += "@" + m + " ";
                    }
                    //// going to reply to @tweeter with live image of each #cameratag separately
                    foreach (Selfies self in selfies)
                    {
                        if (!self.IsActive)
                        {
                            Console.WriteLine("INACTIVE #" + self.HashTag + "@" + self.TwitterID);
                            continue;
                        }

                        //// adds all @mentions to the reply
                        string message = self.Response.Replace("{@mentions}", allmentions.TrimEnd());

                        if (string.IsNullOrEmpty(self.HashTag))
                            self.HashTag = "";

                        bool justTweeted = false;
                        BLL.Entities.Snapshot snap = new BLL.Entities.Snapshot();

                        //// if snapshot cache already has image for this #cameratag then fetch it from there
                        if (_Snapshots.ContainsKey(twitter.ID + self.HashTag))
                        {
                            snap = _Snapshots[twitter.ID + self.HashTag];

                            //// also if cached image is not older then SnapshotCacheLife then use it in tweet reply
                            if (snap.CreatedAt > DateTime.Now.AddSeconds(int.Parse(Utils.SnapshotCacheLife) * -1))
                            {
                                TwitterCredentials.SetCredentials(twitter.AccessToken, twitter.TokenSecret, Utils.ConsumerKey, Utils.ConsumerSecret);
                                Tweet.PublishTweetWithMedia(message, snap.Data);
                                justTweeted = true;
                                Utils.DBLog(LogTypes.Info, "@" + twitter.ID + " [Tweeted-C]: " + message, twid, "Listener");
                            }
                        }

                        //// otherwise just try fetching new image from camera and watermark it and reply the tweet
                        if (!justTweeted)
                        {
                            try
                            {
                                if (!string.IsNullOrEmpty(self.AccessToken))
                                    Evercam = new Evercam(self.AccessToken);
                                else
                                    Evercam = new Evercam(
                                        ConfigurationSettings.AppSettings["EvercamClientID"],
                                        ConfigurationSettings.AppSettings["EvercamClientSecret"],
                                        ConfigurationSettings.AppSettings["EvercamClientUri"]);
                                Camera camera = Evercam.GetCamera(self.CameraID, null);
                                if (camera != null)
                                {
                                    byte[] image = null;
                                    for (int i = 1; i <= TRY_COUNT; i++)
                                    {
                                        try
                                        {
                                            // store and returns live snapshot on evercam
                                            image = Evercam.CreateSnapshot(self.CameraID, ConfigurationSettings.AppSettings["EvercamClientName"], true).ToBytes();
                                            Console.WriteLine("@" + twitter.ID + " (N): Image retrieved (try#" + i + ")" + Environment.NewLine);
                                            //ServerPost("@" + twitter.ID + ": (N) Image retrieved (try#" + i + ")");
                                            break;
                                        }
                                        catch (Exception x)
                                        {
                                            Utils.DBLog(LogTypes.Error, "@" + twitter.ID + " : Image data not retrieved (try#" + i + ") [Error] " + x.ToString(), twid, "Listener");
                                            if (i < TRY_COUNT)
                                                Thread.Sleep(RETRY_INTERVAL);    // 15 seconds
                                        }
                                    }
                                    if (image != null && image.Length > 0)
                                    {
                                        snap.Data = Utils.WatermarkImage(image, self.PhotoText, self.LogoFile, self.TwitterID + self.HashTag);
                                        snap.CreatedAt = DateTime.Now;
                                        snap.HashTag = self.HashTag;
                                        if (snap.Data.Length > 0)
                                        {
                                            if (_Snapshots.ContainsKey(twitter.ID + self.HashTag))
                                                _Snapshots[twitter.ID + self.HashTag] = snap;
                                            else
                                                _Snapshots.Add(twitter.ID + self.HashTag, snap);
                                            TwitterCredentials.SetCredentials(twitter.AccessToken, twitter.TokenSecret, Utils.ConsumerKey, Utils.ConsumerSecret);
                                            Tweet.PublishTweetWithMedia(message, snap.Data);
                                            justTweeted = true;
                                            Utils.DBLog(LogTypes.Info, "@" + twitter.ID + " [Tweeted-N]: " + message, twid, "Listener");
                                        }
                                        else
                                            Utils.DBLog(LogTypes.Info, "@" + twitter.ID + ": No snapshot data", twid, "Listener");
                                    }
                                    else
                                        Utils.DBLog(LogTypes.Debug, "@" + twitter.ID + ": No image data", twid, "Listener");
                                }
                                else
                                    Utils.DBLog(LogTypes.Debug, "@" + twitter.ID + ": Camera not found", twid, "Listener");
                            }
                            catch (Exception x)
                            {
                                //// this is temporarily throwing error to server to restart it
                                //// actually it should be handled here
                                Utils.DBLog(LogTypes.Error, "@" + twitter.ID + " [Error!Tweeted] " + x.ToString(), twid, "Listener");
                                ServerPost("@" + twitter.ID + ": Error: !justTweeted: " + x.Message);
                            }
                            finally
                            {
                                if (!justTweeted)
                                {
                                    if (Utils.TweetWithoutImage == "true")
                                    {
                                        message = "Sorry @" + args.Tweet.Creator.ScreenName + ", due to some problem your photo could not be generated at " + DateTime.UtcNow.ToString() + " (UTC)";
                                        TwitterCredentials.SetCredentials(twitter.AccessToken, twitter.TokenSecret, Utils.ConsumerKey, Utils.ConsumerSecret);
                                        Tweet.PublishTweet(message);
                                        justTweeted = true;
                                        Utils.DBLog(LogTypes.Info, "@" + twitter.ID + " [Tweeted-E] " + message, twid, "Listener");
                                    }
                                }
                            }
                        }

                        //// checks if there was an error in last communication with Twitter
                        if (ExceptionHandler.GetLastException() != null)
                            Utils.DBLog(LogTypes.Error, "@" + twitter.ID + " [Error] " + ExceptionHandler.GetLastException().TwitterExceptionInfos.First().Message, twid, "Listener");
                    }
                }
            }
            else
            {
                Console.WriteLine("Mention Not @" + user.ScreenName);
                return;
            }
        }

        private static string ServerPost(string message)
        {
            clientConnection.Connect();
            clientConnection.Write("@" + twid + ": " + message);
            
            string response = clientConnection.Read();
            clientConnection.Close();

            return response;
        }

        public static string[] UniqueMentions(List<Tweetinvi.Core.Interfaces.Models.Entities.IUserMentionEntity> mentions)
        {
            string[] names = new string[mentions.Count];
            int i = 0;
            foreach (var m in mentions)
            {
                if (!names.Contains(m.ScreenName.ToLower()))
                    names[i++] = m.ScreenName.ToLower();
            }
            return names;
        }

        public static string[] UniqueTags(List<Tweetinvi.Core.Interfaces.Models.Entities.IHashtagEntity> hashtags)
        {
            string[] tags = new string[hashtags.Count];
            int i = 0;
            foreach (var t in hashtags)
            {
                if (!tags.Contains(t.Text.ToLower()))
                    tags[i++] = t.Text.ToLower();
            }
            return tags;
        }
    }
}
