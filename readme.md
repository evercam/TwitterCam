This server application runs a Twitter stream listener which listens to every new tweet posted by the followers of
a particular Twitter user @username (whom credentials are set in config file). 

It locates @username in any tweet's @mention list. If found that, then locates any #cameratags in tweet message. This #cameratag will be searched in application cache first (otherwise will be loaded from DB) to get camera information to fetch live image from. Also the application first look into another cache of camera recent snapshots, before sending request to camera. With this, it significantly reduces hits to camera to fetch images within defined snapshot expiry time in cache (set in app config file). Additionally, if there is no #cameratag mentioned in a tweet, application replies back with image from a default camera (set in app config file).

User can visit http://selfies.evercam.io to add new Selfies (camera detail with new #cameratag). Application is capable of watching at newly added hash tag instantly and will reply back with its camera's live image.

In case, a camera is offline while retrieving live image, application sends back a tweet notifying the initial tweeter that something bad has occured.
